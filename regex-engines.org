#+TITLE: A tale of two regular expression engines
#+OPTIONS: toc:nil date:nil
#+LATEX_CLASS: article
#+LATEX_CLASS_OPTIONS: [twoside, a4paper, 11pt]
#+LATEX_HEADER: \usepackage{a4wide}
#+LATEX_HEADER: \usepackage{baskervald}
#+LATEX_HEADER: \hypersetup{colorlinks=true}

* Introduction

  This document will demonstrate the differences between two regular expression
  (henceforth abreviated to regex) engines, specifically those found in the [[https://www.perl.org/][Perl]]
  and [[https://en.wikipedia.org/wiki/Common_Lisp][Common Lisp]] programming languages. A significant difference between these
  languages (worth noting in the context of this comparison) is that Perl is
  implementation-defined, while Common Lisp is defined as an ANSI standard. As a
  consequence, the Perl regex engine is part of /the/ implementation for the
  language, while =CL-PPCRE= is a [[http://edicl.github.io/cl-ppcre/][portable library]].

* Perl regular expression engine

  \begin{center}
  /The expert realizes that all code is data\ldots/
  \end{center} \\

  The Perl regex engine functions in two distinct phases. During the first
  (compilation) phase, it takes a regex description (in string form) as input
  and produces an optimized internal data representation for it. The second
  (execution) phase is when this data representation is used by an /interpreter/
  in order to match the pattern described by the regex against a target
  string. Schematically:
  * Compilation
    * parsing for size (1st pass)
    * parsing for construction (2nd pass)
    * peep-hole optimization and analysis
  * Execution
    * start position and no-match optimizations
    * program execution
  The following subsections will outline the basic subphases and invoked
  routines (for more, see the [[https://perldoc.perl.org/perlreguts.html][relevant Perl documentation section]]).

** Compilation

   Compilation starts with =pregcomp=, which is a wrapper that sets up the two
   routines doing the heavy lifting, i.e. =reg=, the start point for parsing,
   and =study_chunk=, responsible for optimizations. The =pregcomp= routine
   initializes and fills a special structure (=RExC_state_t=), which contains
   the compilation state and almost all internal routines operate upon.

   Two passes are executed on the input pattern. The first pass is done for
   reconnaissance in order to setup the aforementioned structure (the macro
   =SIZE_ONLY= reduces the functionality of invoked routines to only reporting
   resources needed; throughout the second pass =SIZE_ONLY= is set to
   false). Afterwards, the second pass is when the allocated structure is
   appropriately filled by many routines (=reg=, =regbranch=, =regatom=,
   =regpiece=, =regtail=, etc.) that call each other as they parse the string
   representation of the regex.

   After the two passes described above, the =study_chunk= routine is invoked,
   which performs optimizations on the constructed =RExC_state_t= structure,
   while storing analysis data to a special =scan_data_t= structure. At the end
   of this phase, the resulting data structure is a graph of interconnected
   nodes (called regops) which determines the flow of the matching process. For
   example, the pattern =/foo(?:\w+|\d+|\s+)bar/= can be thought to be
   translated to the following regop graph:

   #+BEGIN_EXAMPLE
                   +-- <\w+> --+
                   |           |
[start] -- <foo> --+-- <\d+> --+-- <bar> -- [end]
                   |           |
                   +-- <\s+> --+
   #+END_EXAMPLE

** Execution
   The execution phase is again split into two subphases, namely finding the
   string point where the matching process should start from and running the
   regop interpreter. The analysis stage of the compilation phase will provide,
   along with short-cut algorithms to the match starting point, some preliminary
   checks for whether the interpreter should be run at all, as it is sometimes
   provably impossible for the regex to match (or, equivalently, to find a valid
   starting point for the matching process).

   The main regop interpreter frontend is the =pregexec= routine, which
   maintains an internal stack (allocated on the heap) in order to avoid
   explicit recursive invocations of itself. It is called by =re_intuit_start=,
   which is responsible for handling start points and no-match optimizations as
   determined by the =study_chunk= analysis. The entry to the regex interpreter
   itself is =regtry=, which acts as a set-up wrapper around =regmatch=, which
   in turn implements the main "recursive loop" of the interpeter. It can be
   thought of as a giant switch statement that implements a state machine, where
   the possible states are the regops themselves, plus a number of additional
   intermediate and failure states. A few of the states are implemented as
   subroutines but the bulk are inline code.

* Common Lisp Perl-compatible regular expression library

  \begin{center}
  /\ldots whereas the true master sees that all data is code./
  \end{center} \\

  Conversely, =CL-PPCRE= adopts a radically different approach to regex
  matching. It treats the regex string description as an algorithm described in
  a mini-language (the regex syntax specification in this case), compiling it
  down to chains of closures, which in turn get translated to machine code.

  The creation of a scanner function starts with the deserialization (lexing and
  parsing) of the regex string into a =PARSE-TREE=. For example, the regex
  ="a|b??[c-e]{2,4}|\\d"= is parsed as follows:

  #+BEGIN_SRC lisp :session :exports none
(ql:quickload '(cl-ppcre closer-mop))
(in-package :cl-ppcre)
  #+END_SRC

  #+RESULTS:
  : #<PACKAGE "CL-PPCRE">

  #+BEGIN_SRC lisp :session :exports both
(defparameter *regex* "(a|b??([c-e]{2,4})|\\d)\\1$")
(ppcre:parse-string  *regex*)
  #+END_SRC

  #+RESULTS:
  : (:SEQUENCE
  :  (:REGISTER
  :   (:ALTERNATION #\a
  :    (:SEQUENCE (:NON-GREEDY-REPETITION 0 1 #\b)
  :     (:REGISTER (:GREEDY-REPETITION 2 4 (:CHAR-CLASS (:RANGE #\c #\e)))))
  :    :DIGIT-CLASS))
  :  (:BACK-REFERENCE 1) :END-ANCHOR)

  This =PARSE-TREE= is then passed onto the =CREATE-SCANNER= function, which
  creates closures that parse regex elements as it traverses its tree. The
  closures are created by the generic function =CREATE-MATCHER-AUX=, whose
  methods

  #+BEGIN_SRC lisp :session :exports both
(sb-mop:generic-function-methods #'ppcre::create-matcher-aux)
  #+END_SRC

  #+RESULTS:
  #+begin_example
  (#<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (REPETITION T) {10016B74A3}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (VOID T) {10016B7533}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (FILTER T) {10016B7543}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (STANDALONE T) {10016B7553}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (BRANCH T) {10016B7563}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (BACK-REFERENCE
                                                   T) {10016B7573}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (ANCHOR T) {10016B7583}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (EVERYTHING T) {10016B7593}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (WORD-BOUNDARY T) {10016B75A3}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (STR T) {10016B75B3}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (CHAR-CLASS T) {10016B75C3}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (LOOKBEHIND T) {10016B75D3}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (LOOKAHEAD T) {10016B75E3}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (REGISTER T) {10016B75F3}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (ALTERNATION T) {10016B7603}>
   #<STANDARD-METHOD CL-PPCRE::CREATE-MATCHER-AUX (SEQ T) {10016B7613}>)
  #+end_example
  match the tags present in the =PARSE-TREE= resulting from =parse-string=. This
  kind of compilation into a graph of closures is well-established in Lisp
  environments, as =READ=, =EVAL= and =COMPILE= are deeply intertwined in time
  and space. Closures are first-class objects of the language and as such can be
  generated, referenced, invoked, deleted, stored in data structures, passed as
  arguments, etc. The same approach has been demonstrated also in the book [[http://www.paulgraham.com/onlisp.html][On
  Lisp]] by Paul Graham. The 6th chapter "Functions as Representation" ([[http://ep.yimg.com/ty/cdn/paulgraham/onlisp.pdf][pdf]]
  available online for free), provides an introduction to this technique, where
  network nodes are represented by closures which implement the graph traversal
  operations. More advanced usage can be found in chapters 23 and 24, which
  describe an Augmented Transition Networks (ATNs) framework for
  non-deterministic parsing of natural language and a rudimentary Prolog
  implementation as an embedded language, respectively. Finally, let over
  lambda, a book from Doug Hoyte partially available online. contains a [[https://letoverlambda.com/index.cl/guest/chap4.html#sec_4][section]]
  about =CL-PPCRE=.

* To match or not to match: a case study

  \begin{center}
  /\ldots or why implementing declarative languages is hard./
  \end{center} \\

  On release of =CL-PPCRE=, [[http://web.archive.org/web/20080624164217/http://weitz.de/cl-ppcre/#bench][benchmarks]] showed that it outperformed the Perl
  regex engine by a factor of 2 in execution time. These claims have since been
  removed, although objections against both algorithm and implementation of the
  Perl regex engine have been raised elsewhere. A notable example is the [[https://swtch.com/~rsc/regexp/regexp1.html][first]]
  of a [[https://swtch.com/~rsc/regexp/regexp2.html][four]] [[https://swtch.com/~rsc/regexp/regexp3.html][article]] [[https://swtch.com/~rsc/regexp/regexp4.html][series]] by [[https://swtch.com/~rsc/][Russ Cox]], where the performance on "pathological
  regex" is investigated. This regex ($a?^na^n$ -- superscripts indicate
  repetition, i.e. $a?^3a^3$ stands for $a?a?a?aaa$) exploits a weakness in the
  matching strategy, in what essentially amounts to the difference between
  depth-first search (DFS) and breadth-first search (BFS). The Perl regex engine
  match shows exponential growth with respect to the $n$ parameter mentioned
  above, as it tries to match the regex with a DFS strategy based on
  backtracking. This is still true even today:

  #+BEGIN_SRC perl :exports both :results output
use Time::HiRes;

sub MatchTime {
    my $n = @_[0];
    my $target = "a" x $n;
    my $regex = "a?" x $n . $target;
    my $start = Time::HiRes::gettimeofday();
    $target =~ $regex;
    my $end = Time::HiRes::gettimeofday();
    return $end - $start;
}

foreach $n (23, 24, 25, 26, 27) {
    print "n = $n, time: ", MatchTime($n), "\n";
}
  #+END_SRC

  #+RESULTS:
  : n = 23, time: 0.307670116424561
  : n = 24, time: 0.617727041244507
  : n = 25, time: 1.23452496528625
  : n = 26, time: 2.47192406654358
  : n = 27, time: 5.21877503395081

  Interestingly enough, =CL-PPCRE= is not susceptible to this problem:

  #+BEGIN_SRC lisp :session :exports both :results output
(flet ((match-time (n)
         (let ((target (format nil "~v@{~a~:*~}" n "a"))
               (regex (format nil "~v{~a~:*~}~v{~a~:*~}"
                              n '("a?") n '("a")))
               (*trace-output* *standard-output*))
           (format t "N = ~a~%" n)
           (time (ppcre:scan regex target)))))
  (mapc #'match-time '(1000 10000)))
  #+END_SRC

  #+RESULTS:
  #+begin_example
  N = 1000
  Evaluation took:
    0.001 seconds of real time
    0.000900 seconds of total run time (0.000808 user, 0.000092 system)
    100.00% CPU
    3,057,440 processor cycles
    585,600 bytes consed

  N = 10000
  Evaluation took:
    0.009 seconds of real time
    0.008729 seconds of total run time (0.008729 user, 0.000000 system)
    100.00% CPU
    29,733,112 processor cycles
    6,044,512 bytes consed

  #+end_example
  This is because =CL-PPCRE= checks for a constant end string and searches for
  it in the spirit of the good suffix rule in Boyer-Moore matchers[fn:bmh]. In
  both contexts, this is analogous to search space pruning (continuing along the
  DFS / BFS metaphor), reducing the number of operations needed. Although
  [[http://perlmonks.org/index.pl?node_id=502408][benchmarks aren't everything]], and implementing a mini-language undoubtedly
  involves a lot of trade-offs, this is a pretty convincing example of some
  weaknesses that can be found in production-quality regex engines.

[fn:bmh] Boyer-Moore-Horspool (BMH) matchers can be used for matching strings in
=CL-PPCRE= instead of the standard Common Lisp function =SEARCH= by setting the
special variable =*use-bmh-matchers*= to =T=. This is by default set to =NIL=
because BMH matchers are faster but need much more space (more information on
BMH matchers can be found in [[https://en.wikipedia.org/wiki/Boyer%25E2%2580%2593Moore%25E2%2580%2593Horspool_algorithm][Wikipedia]] or [[http://www-igm.univ-mlv.fr/~lecroq/string/node18.html][here]]).

* Conclusion

  A rough overview of the aforementioned regex engines reveals deep differences
  in implementation methodology and design decisions. Programmers of the Perl
  engine had to devise a regop interpreter and shoehorn operations and
  optimizations into the regop structure[fn:greenspun]. On the other hand,
  =CL-PPCRE= regexes are translated to Lisp programs, generated by appropriate
  methods and compiled by the Lisp system. This results in a very readable
  implementation, even in optimization-heavy code, which is definitely not the
  case with the Perl engine[fn:perl-warning].

  If we were to generalize, we'd say that Lisp, being the kernel of every
  language, provides the means to describe a solution in the way most natural to
  the problem at hand. In this case, the translation of finite automata to
  graphs of closures through a clean, object-oriented design in a
  performance-sensitive manner is something quite remarkable. In principle,
  regex specifications are nothing but programs written in their own
  mini-language, with no reason whatsoever preventing them from being compiled
  to any other suitable representation. The degree to which the host environment
  can assimilate the guest language is what makes all the difference.

[fn:greenspun] Greenspun's tenth rule of programming: "Any sufficiently
complicated C or Fortran program contains an ad-hoc, informally-specified,
bug-ridden, slow implementation of half of Common Lisp".
[fn:perl-warning] To quote the perlreguts documentation page: "The code involved
in =study_chunk()= is extremely cryptic. Be careful. :-)".

# Local variables:
# indent-tabs-mode: nil
# End:
